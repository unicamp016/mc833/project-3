import os
import glob
import argparse
import subprocess

import pylab
import numpy as np
import scipy.stats as stats
import matplotlib.pyplot as plt

# Logs data
log = True

# Directories
bin_dir = "bin"
build_dir = 'build'
log_dir = "logs"

# Filenames
stats_fn = "stats.txt"
dummy_fn = "dummy.txt"
client_fn = "cli_time.txt"
database_fn = "proc_time.txt"

cities = [
        'Campinas',
        'Berlin',
        'Ouroeste'
        ]

courses = [
        'Computer Engineering',
        'Archeology'
        ]

emails = [
        'juja.princess@gmail.com',
        'panda.prudencio@gmail.com',
        'mimao.vendra@hotmail.com',
        'tutti.nervouser@yahoo.com'
        ]

experiences = [
        'This is a sample experience',
        'This is another experience',
        'More experience!'
        ]

def create_dummy_input_single(tp, option):
    i = 0
    if tp == 'rmi':
        d = build_dir
    else:
        d = bin_dir

    with open(os.path.join(tp, d, f'{tp}_{dummy_fn}'), "w") as f:
        f.write(f"{option}\n")

        if option == 1:
            cur_course = courses[i%len(courses)]
            f.write(f"{cur_course}\n")

        elif option == 2:
            cur_city = cities[i%len(cities)]
            f.write(f"{cur_city}\n")

        elif option == 3:
            cur_email = emails[i%len(emails)]
            cur_exp = experiences[i%len(experiences)]
            f.write(f"{cur_email}\n")
            f.write(f"{cur_exp}\n")

        elif option == 4 or option == 6:
            cur_email = emails[i%len(emails)]
            f.write(f"{cur_email}\n")

        f.write("0\n");

def write_proc_times(option, n, host='localhost', user=None, remote_dir=None):

    # Create dummy file with a single request
    create_dummy_input_single('tcp', option)
    create_dummy_input_single('rmi', option)

    # Compute n processing times for tcp
    tcp_dummy_fn = os.path.join('tcp', bin_dir, f'tcp_{dummy_fn}')
    for i in range(n):
        cmd = f"cat {tcp_dummy_fn} | make -C tcp run_client HOST={host}"
        subprocess.run(cmd, shell=True)

    if option == 3:
        if host == 'localhost':
            cmd = "make reset_db"
        else:
            cmd = f"ssh {user}@{host} 'cd {remote_dir}; make reset_db'"
        subprocess.run(cmd, shell=True)

    # Compute n processing times for rmi
    rmi_dummy_fn = os.path.join('rmi', build_dir, f'rmi_{dummy_fn}')
    for i in range(n):
        cmd = f"cat {rmi_dummy_fn} | make -C rmi run_client HOST={host}"
        subprocess.run(cmd, shell=True)

    if option == 3:
        if host == 'localhost':
            cmd = "make reset_db"
        else:
            cmd = f"ssh {user}@{host} 'cd {remote_dir}; make reset_db'"
        subprocess.run(cmd, shell=True)

def get_proc_times(option, server_type, plot_name=''):


    prefix = f'{server_type}'

    cl_filename = os.path.join(server_type, log_dir, f'{prefix}_{client_fn}')
    db_filename = os.path.join(server_type, log_dir, f'{prefix}_{database_fn}')

    cl_file = open(cl_filename, 'r')
    db_file = open(db_filename, 'r')

    cl_times, db_times = [], []
    db_lines = db_file.readlines()
    for i, line in enumerate(cl_file.readlines()):
        tokens = line.strip().split(' ')
        if int(tokens[0]) == option:
            cl_times.append(float(tokens[-1]))
            db_times.append(float(db_lines[i].strip().split(' ')[-1]))

    cl_file.close()
    db_file.close()

    return np.array(db_times), np.array(cl_times)

def plot_data(server_type, plot_name=''):
    for option in range(1, 7):
        db_times, cl_times = get_proc_times(option, server_type, plot_name)

        plot_gaussian(cl_times-db_times, option, server_type, "Tempo de Comunicação", plot_name)
        plot_gaussian(cl_times, option, server_type, "Tempo do Cliente", plot_name)
        plot_gaussian(db_times, option, server_type, "Tempo de Processamento", plot_name)

def plot_gaussian(proc_times, option, server_type, name, plot_name):

    # Compute sample mean and sample standard deviation estimate
    mu = np.mean(proc_times)
    sigma = stats.tstd(proc_times)
    interv = (mu-sigma*1.96, mu+sigma*1.96)

    # Get plot filename from plot title.
    if name == 'Tempo de Comunicação':
        plt_name = 'communication'
    elif name == 'Tempo do Cliente':
        plt_name = 'client'
    elif name == 'Tempo de Processamento':
        plt_name = 'proc'

    # plt_name = '_'.join(map(str.lower, name.split(' ')))

    # Create just one figure and one subplot
    fig, ax = plt.subplots(figsize=(10,10))

    # Plot pdf relative likelihood for each point
    pdf = stats.norm.pdf(proc_times, mu, sigma)
    ax.scatter(proc_times, pdf, color=(1.0, 0.0, 0.0, 1.0))

    # Clip min and max y values
    ax.set_ylim(0, 1.1*max(pdf))

    # Create a new curve that represents a 95% confidence interval
    ci_xs = np.linspace(interv[0], interv[1], 30)
    ci_ys = stats.norm.pdf(ci_xs, mu, sigma)
    ax.fill_between(ci_xs, ci_ys, facecolor=(1.0, 0.0, 0.0, 0.4), label='95% IC')

    ax.legend()
    ax.set_ylabel('$f(x)$')
    ax.set_xlabel(f'{name} (ms)')

    if len(plot_name) > 0:
        prefix = f'{plot_name}-'
    else:
        prefix = ''
    fig.savefig(f'bin/{prefix}{server_type}-{option}-{plt_name}.pdf')
    plt.close(fig)

    # Prints to log file if required
    if log:
        with open(os.path.join('bin', f'{prefix}{stats_fn}'), 'a') as st:
            st.write(f"{name}\n")
            st.write(f"Server type: {server_type}\n")
            st.write(f"Option: {option}\n")
            st.write(f"Mean: {mu}\n")
            st.write(f"Standard Deviation: {sigma}\n")
            st.write(f"Interval: {interv}\n")

def plot_stderr(type='client', plot_name=''):
    if len(plot_name) > 0:
        prefix = f'{plot_name}-'
    else:
        prefix = ''
    filename = os.path.join(bin_dir, f'{prefix}{stats_fn}')

    # Store 3-uple with mean, lower, and upper interval
    xs = []
    tcp_xs, tcp_ys, tcp_yerrs = [], [], []
    rmi_xs, rmi_ys, rmi_yerrs = [], [], []

    if type == 'communication':
        tcp_start = 0
        rmi_start = 108
    elif type == 'client':
        tcp_start = 6
        rmi_start = 114
    else:
        tcp_start = 12
        rmi_start = 120

    with open(filename, 'r') as f:

        lines = f.readlines()

        option = 0
        while option < 6:
            tcp_index = tcp_start + 18*option
            rmi_index = rmi_start + 18*option

            tcp_mean = float(lines[tcp_index+3].split(':')[-1])
            tcp_sigma = float(lines[tcp_index+4].split(':')[-1])
            rmi_mean = float(lines[rmi_index+3].split(':')[-1])
            rmi_sigma = float(lines[rmi_index+4].split(':')[-1])

            option_id = option + 1
            xs.append(option_id)
            tcp_xs.append(option_id - 0.1)
            tcp_ys.append(tcp_mean)
            tcp_yerrs.append(tcp_sigma*1.96)
            rmi_xs.append(option_id + 0.1)
            rmi_ys.append(rmi_mean)
            rmi_yerrs.append(rmi_sigma*1.96)

            option += 1

        # Plot graph with given stats
        fig, ax = plt.subplots(figsize=(10, 10))
        ax.errorbar(tcp_xs, tcp_ys, tcp_yerrs, linestyle='None', ecolor=(1.0, 0.0, 0.0, 0.4))
        ax.scatter(tcp_xs, tcp_ys, label='TCP', color=(1.0, 0.0, 0.0, 0.8))
        ax.errorbar(rmi_xs, rmi_ys, rmi_yerrs, linestyle='None', ecolor=(0.0, 0.0, 1.0, 0.4))
        ax.scatter(rmi_xs, rmi_ys, label='RMI', color=(0.0, 0.0, 1.0, 0.8))
        ax.legend()
        ax.set_ylim(0, 1.1*np.max(tcp_ys + rmi_ys + tcp_yerrs + rmi_yerrs))
        ax.set_xlim(np.min(xs)-1, np.max(xs)+1)
        ax.set_ylabel('Tempo de Resposta (ms)')
        ax.set_xlabel('Requisição')

        if len(plot_name) > 0:
            prefix = f'{plot_name}-{type}'
        else:
            prefix = f'{type}'
        fig.savefig(f'bin/{prefix}-stderr.pdf')
        plt.close(fig)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--iterations', type=int, required=False, default=20,
                        help='Number of samples to run for each request type.')
    parser.add_argument('--host', required=False, default='localhost',
                        help='IP address of running servers')
    parser.add_argument('--user', required=False, default=None,
                        help='User for ssh connection')
    parser.add_argument('--remote_dir', required=False, default=None,
                        help='Remote directory where database files are located on server')
    parser.add_argument('--plot', action='store_true',
                        help='Plot data given that all log files are available.')
    parser.add_argument('-n', '--plot_name', default='',
                        help='Log filenames prefix to use for plotting graphs.')

    args = parser.parse_args()

    if args.host == 'localhost' and not args.plot:
        # Clear log files
        log_files = glob.glob("*/logs/*") + glob.glob("bin/*stats.txt")
        for lf in log_files:
            open(lf, "w").close()

        for option in range(1, 7):

            # Run experiments and write processing times to files
            write_proc_times(option, args.iterations, args.host)

        # Plot gaussian distribution for data
        plot_data('tcp', plot_name=args.plot_name)
        plot_data('rmi', plot_name=args.plot_name)

        # Plot standard error comparison between server types
        plot_stderr('client', plot_name=args.plot_name)
        plot_stderr('communication', plot_name=args.plot_name)

    else:
        if not args.plot:
            if args.user is None or args.remote_dir is None:
                raise ValueError("ERROR: When server is on a remote IP, a"
                "user and a remote_dir must be specified")

            # Clear log files
            log_files = glob.glob("*/logs/*")
            for lf in log_files:
                open(lf, "w").close()

            for option in range(1, 7):

                # Write processing times to files
                write_proc_times(option, args.iterations, args.host, args.user, args.remote_dir)

        else:
            # Clear stats file
            open(os.path.join('bin', stats_fn), "w").close()

            # Plot gaussian distribution for data
            plot_data('tcp', plot_name=args.plot_name)
            plot_data('rmi', plot_name=args.plot_name)

            # Plot standard error comparison between server types
            plot_stderr('client', plot_name=args.plot_name)
            plot_stderr('communication', plot_name=args.plot_name)
