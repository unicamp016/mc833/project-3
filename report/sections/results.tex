\section{Método Experimental}\label{sec:method}
  Para medir o desempenho da comunicação cliente/servidor, foram coletadas \iterations~amostras do tempo total de consulta/atualização vistas pelo cliente ($T_t$) e do tempo de processamento para cada operação do ponto de vista do servidor ($T_p$). Como o tempo de processamento no servidor não está relacionado ao protocolo de comunicação, esse tempo é subtraido do tempo total do ponto de vista do cliente para obter uma estimativa do tempo de comunicação ($T_c = T_t - T_p$).

  As amostras foram coletadas sem concorrência, ou seja, havia um único cliente fazendo as requisições. O sistema foi testado de maneira automatizada, onde um arquivo com os comandos desejados é redirecionado para a entrada padrão do cliente. Tanto para o TCP como o RMI, os testes foram feitos com conexões separadas. Isto é, toda requisição é seguida pelo comando 0 para terminar a conexão. O tempo de estabelecer a conexão não é levado em consideração. Para cada tipo de requisição os mesmos argumentos foram usados e para a requisição 6, foram feitos testes com um perfil que possui uma foto.

  Foram realizados dois tipos de testes: em LAN usando rede cabeada com protocolo de enlace Ethernet e em LAN usando rede sem fio com protocolo de enlace Wi-Fi, ambos com o cliente e servidor cujas especificações são dadas pela Tabela~\ref{tab:pc-stats}. Como o banco de dados está armazenado no servidor, devemos levar em conta a especificação de disco do servidor ao avaliar seu desempenho.

  \begin{table}[!ht]
      \begin{center}
        \caption{Especificação das máquinas dos clientes e servidor usadas para realizar os experimentos.}\label{tab:pc-stats}
        \resizebox{\textwidth}{!}{
        \begin{tabular}{|c|c|c|}
            \hline
            \textbf{Tipo} & \textbf{Cliente} & \textbf{Servidor}\\
            \hline
            CPU & Intel Core i5-7300HQ & Intel Core i7-3770K \\
            Clock & 2.50GHz & 3.50GHz\\
            Cores & 4 & 4\\
            RAM & 8GB 2400MHz & 32GB 1600 MHz \\
            Disco secundário & SSD & SSD \\
            Interface de disco  & SATA 6GB & SATA 6GB \\
            Protocolo Wi-Fi & 802.11ac & 802.11n \\
            Ethernet (capacidade) & 1Gbit/s & 1Gbit/s \\
            \hline
            \end{tabular}
          }
      \end{center}
  \end{table}

\section{Resultados}
  Os resultados dos experimentos detalhados na Seção~\ref{sec:method} estão dispostos nas Tabelas~\ref{tab:eth-metrics} e~\ref{tab:wifi-metrics}. A partir das medidas dos tempos $T_c$, $T_p$ e $T_t$ de cada requisição, foram gerados os gráficos das Figuras~\ref{fig:eth-stderr} e~\ref{fig:wifi-stderr}. A seguir será apresentada uma discussão dos principais resultados dos experimentos.

    \subsection{Nível de Abstração e Tamanho de Código}
	A diferença de tamanho de código entre ambos servidores não é uma boa métrica neste caso pois há diferenças além do método utilizado: linguagem e paradigma de programação. O servidor TCP está implementado na linguagem C, no paradigma procedural, enquanto o servidor RMI está implementado na linguagem Java, no paradigma orientado a objetos.

	Por outro lado, há diferenças no nível de abstração entre ambos servidores que são pertinentes. A principal vantagem encontrada no RMI é sua simplicidade de implementação dada pelo alto nível de abstração: a API do Java\cite{RMI} tem todo o protocolo já implementado, e assim cabe ao programador apenas fazer algumas configurações, estruturar o formato das mensagens de resposta (serializáveis) e explicitar qual o método a ser invocado remotamente. Toda a comunicação, incluindo confiabilidade e outras condições da camada de transporte, é garantida pela biblioteca: os argumentos para o método são enviados ao servidor, e o valor de retorno é enviado ao cliente. A implementação do cliente é ainda mais simples, precisando apenas saber como encontrar o servidor dado o \textit{host}. Após o início, a comunicação se dá de forma transparente, como a invocação de um método localmente.

	Enquanto isso, no servidor TCP a programação ocorre em um nível mais baixo, lidando diretamente com abertura e gerenciamento de \textit{sockets}, e manualmente enviando e recebendo informações quando necessário. Isso pode ser bom, a depender da implementação, pois permite receber e enviar informações durante o processamento da requisição inicial, enquanto em RMI há apenas o envio da requisição e o retorno da resposta uma única vez. Esse nível de abstração mais baixo também faz com que o programador tenha que se preocupar mais com aspectos da comunicação que são garantidos em RMI, tirando o foco do processamento da requisição.

    \subsection{Discussão}
    O tempo de resposta para uma requisição é uma variável aleatória contínua. O nosso objetivo é estimar a média da variável e encontrar o intervalo de confiança para essa estimativa da média. Usando o Teorema do Limite Central e assumindo uma distribuição normal, podemos estimar o valor da média e o erro padrão usando um conjunto de amostras através das Equações~\ref{eq:sample-mean} e~\ref{eq:sample-stdev}.

    \begin{equation}\label{eq:sample-mean}
        \mu_x = \frac{1}{n} \sum_{i=1}^n x_i
    \end{equation}

    \begin{equation}\label{eq:sample-stdev}
        \sigma_x = \frac{\sigma}{\sqrt{n}}
    \end{equation}

    Entretanto, como pode ser visto na Equação~\ref{eq:sample-stdev}, é necessário conhecer o desvio padrão da população ($\sigma$) para encontrar o erro padrão ($\sigma_x$). É claro que no caso dos tempos de resposta das requisições, esse parâmetro da população é desconhecido. Dessa forma, podemos optar por usar uma distribuição t de Student para computar o intervalo de confiança, que não requer o conhecimento prévio do desvio padrão da população, ou podemos usar uma estimativa para o desvio padrão da média. Por simplicidade, optamos pela segunda opção, onde usamos um estimador não enviesado para variância, do qual obtemos o erro padrão corrigido que é dada pela Equação~\ref{eq:unbiased-sample-stdev}~\cite{walpole}.

    \begin{equation}\label{eq:unbiased-sample-stdev}
        s_x = \sqrt{\frac{1}{n-1} \sum_{i=1}^n (x_i - \mu_x)^2}
    \end{equation}

    Usando a nossa estimativa para o erro padrão ($s_x$), podemos encontrar um intervalo de confiança de 95\% da média, isto é, um intervalo que tem 95\% de chance de conter a média real da população. Através da tabela para a função acumulada da distribuição normal, identificamos que o $Z$-score correspondente a esse intervalo de confiança é $1,96$. Portanto, podemos dizer que o intervalo $[\mu-1,96s_x, \mu+1,96s_x]$ tem 95\% de chance de conter a média da população. Os tempos $T_t$, $T_p$ e $T_c$ de cada teste estão dispostos nas Tabelas~\ref{tab:eth-metrics} e~\ref{tab:wifi-metrics}, com a média, erro padrão e intervalo de confiança para cada variável de cada operação.

  \begin{table}[]
      \begin{center}
          \caption{Estimativa do tempo médio do cliente, comunicação e processamento para todas as requisições de cada tipo de servidor usando um enlace Ethernet testado em uma rede LAN com um intervalo de confiança de 95\%.}\label{tab:eth-metrics}
          \resizebox{\textwidth}{!}{
          \begin{tabular}{|c|c|c|c|c|c|}
          \hline
          \textbf{Servidor} & \textbf{Requisição} & \textbf{Tipo} & \boldmath $\mu_x$ \textbf{(ms)} & \boldmath $s_x$ \textbf{(ms)} & \textbf{IC de 95\% (ms)}\\
          \hline
          \multirow{18}{*}{ TCP} & \multirow{3}{*}{1} & $T_c$ & 0.618 & 0.074 & [0.472; 0.764] \\
		  &    & $T_t$ & 0.986 & 0.126 & [0.738; 1.234] \\
		  &    & $T_p$ & 0.368 & 0.065 & [0.240; 0.496] \\
		  \cline{2-6}
		  & \multirow{3}{*}{2} & $T_c$ & 0.578 & 0.372 & [0.000; 1.308] \\
		  &    & $T_t$ & 0.962 & 0.375 & [0.227; 1.698] \\
		  &    & $T_p$ & 0.384 & 0.054 & [0.278; 0.489] \\
		  \cline{2-6}
		  & \multirow{3}{*}{3} & $T_c$ & 0.495 & 0.143 & [0.215; 0.775] \\
		  &    & $T_t$ & 2.328 & 1.932 & [0.000; 6.115] \\
		  &    & $T_p$ & 1.833 & 1.922 & [0.000; 5.600] \\
		  \cline{2-6}
		  & \multirow{3}{*}{4} & $T_c$ & 0.495 & 0.090 & [0.318; 0.671] \\
		  &    & $T_t$ & 0.936 & 0.164 & [0.614; 1.258] \\
		  &    & $T_p$ & 0.441 & 0.122 & [0.201; 0.681] \\
		  \cline{2-6}
	  	  & \multirow{3}{*}{5} & $T_c$ & 2.040 & 0.161 & [1.724; 2.357] \\
		  &    & $T_t$ & 4.479 & 0.750 & [3.009; 5.949] \\
		  &    & $T_p$ & 2.439 & 0.653 & [1.160; 3.718] \\
		  \cline{2-6}
		  & \multirow{3}{*}{6} & $T_c$ & 1.414 & 0.115 & [1.189; 1.639] \\
		  &    & $T_t$ & 2.370 & 0.215 & [1.948; 2.792] \\
		  &    & $T_p$ & 0.956 & 0.146 & [0.669; 1.243] \\
		  \cline{2-6}
		  \hline
		  \multirow{18}{*}{ RMI} & \multirow{3}{*}{1} & $T_c$ & 13.403 & 3.685 & [6.182; 20.625] \\
		  &    & $T_t$ & 15.020 & 5.841 & [3.572; 26.468] \\
		  &    & $T_p$ & 1.617 & 3.866 & [0.000; 9.195] \\
		  \cline{2-6}
		  & \multirow{3}{*}{2} & $T_c$ & 16.551 & 24.033 & [0.000; 63.656] \\
		  &    & $T_t$ & 17.675 & 24.016 & [0.000; 64.746] \\
		  &    & $T_p$ & 1.124 & 0.256 & [0.623; 1.625] \\
		  \cline{2-6}
		  & \multirow{3}{*}{3} & $T_c$ & 13.641 & 4.080 & [5.643; 21.638] \\
		  &    & $T_t$ & 16.462 & 4.122 & [8.384; 24.541] \\
		  &    & $T_p$ & 2.822 & 0.914 & [1.030; 4.613] \\
		  \cline{2-6}
		  & \multirow{3}{*}{4} & $T_c$ & 14.088 & 5.865 & [2.593; 25.583] \\
		  &    & $T_t$ & 15.017 & 5.845 & [3.562; 26.473] \\
		  &    & $T_p$ & 0.929 & 0.191 & [0.555; 1.303] \\
		  \cline{2-6}
		  & \multirow{3}{*}{5} & $T_c$ & 217.957 & 37.997 & [143.484; 292.430] \\
		  &    & $T_t$ & 269.419 & 39.820 & [191.370; 347.467] \\
		  &    & $T_p$ & 51.461 & 8.162 & [35.463; 67.460] \\
		  \cline{2-6}
		  & \multirow{3}{*}{6} & $T_c$ & 150.016 & 45.418 & [60.997; 239.034] \\
		  &    & $T_t$ & 187.087 & 45.991 & [96.945; 277.229] \\
		  &    & $T_p$ & 37.071 & 2.630 & [31.916; 42.226] \\
		  \cline{2-6}
  		  \hline
          \end{tabular}
          }
      \end{center}
  \end{table}

  \begin{table}[]
      \begin{center}
          \caption{Estimativa do tempo médio do cliente, comunicação e processamento para todas as requisições de cada tipo de servidor usando um enlace Wi-Fi testado em uma rede LAN com um intervalo de confiança de 95\%.}\label{tab:wifi-metrics}
          \resizebox{\textwidth}{!}{
          \begin{tabular}{|c|c|c|c|c|c|}
          \hline
          \textbf{Servidor} & \textbf{Requisição} & \textbf{Tipo} & \boldmath $\mu_x$ \textbf{(ms)} & \boldmath $s_x$ \textbf{(ms)} & \textbf{IC de 95\% (ms)}\\
          \hline
		  \multirow{18}{*}{ TCP} & \multirow{3}{*}{1} & $T_c$ & 12.478 & 25.332 & [0.000; 62.128] \\
		  &    & $T_t$ & 12.920 & 25.339 & [0.000; 62.584] \\
		  &    & $T_p$ & 0.442 & 0.068 & [0.309; 0.575] \\
		  \cline{2-6}
		  & \multirow{3}{*}{2} & $T_c$ & 12.568 & 31.385 & [0.000; 74.082] \\
		  &    & $T_t$ & 13.067 & 31.390 & [0.000; 74.592] \\
		  &    & $T_p$ & 0.499 & 0.088 & [0.326; 0.671] \\
		  \cline{2-6}
		  & \multirow{3}{*}{3} & $T_c$ & 6.371 & 2.571 & [1.331; 11.411] \\
		  &    & $T_t$ & 8.028 & 2.584 & [2.962; 13.093] \\
		  &    & $T_p$ & 1.656 & 0.361 & [0.948; 2.364] \\
		  \cline{2-6}
		  & \multirow{3}{*}{4} & $T_c$ & 12.308 & 22.401 & [0.000; 56.214] \\
		  &    & $T_t$ & 12.767 & 22.419 & [0.000; 56.708] \\
		  &    & $T_p$ & 0.458 & 0.097 & [0.268; 0.648] \\
		  \cline{2-6}
		  & \multirow{3}{*}{5} & $T_c$ & 85.118 & 14.169 & [57.347; 112.890] \\
		  &    & $T_t$ & 88.489 & 14.337 & [60.388; 116.590] \\
		  &    & $T_p$ & 3.371 & 1.378 & [0.669; 6.072] \\
		  \cline{2-6}
		  & \multirow{3}{*}{6} & $T_c$ & 41.915 & 33.430 & [0.000; 107.437] \\
		  &    & $T_t$ & 42.972 & 33.442 & [0.000; 108.519] \\
		  &    & $T_p$ & 1.057 & 0.172 & [0.720; 1.394] \\
		  \cline{2-6}
		  \hline
		  \multirow{18}{*}{ RMI} & \multirow{3}{*}{1} & $T_c$ & 15.746 & 15.384 & [0.000; 45.898] \\
		  &    & $T_t$ & 17.387 & 16.000 & [0.000; 48.748] \\
		  &    & $T_p$ & 1.641 & 3.514 & [0.000; 8.529] \\
		  \cline{2-6}
		  & \multirow{3}{*}{2} & $T_c$ & 14.735 & 8.060 & [0.000; 30.532] \\
		  &    & $T_t$ & 15.905 & 8.036 & [0.155; 31.655] \\
		  &    & $T_p$ & 1.170 & 0.244 & [0.692; 1.647] \\
		  \cline{2-6}
		  & \multirow{3}{*}{3} & $T_c$ & 14.496 & 7.580 & [0.000; 29.352] \\
		  &    & $T_t$ & 17.122 & 7.600 & [2.227; 32.017] \\
		  &    & $T_p$ & 2.626 & 0.313 & [2.012; 3.240] \\
		  \cline{2-6}
		  & \multirow{3}{*}{4} & $T_c$ & 14.463 & 8.735 & [0.000; 31.584] \\
		  &    & $T_t$ & 15.400 & 8.732 & [0.000; 32.516] \\
		  &    & $T_p$ & 0.937 & 0.249 & [0.449; 1.425] \\
		  \cline{2-6}
		  & \multirow{3}{*}{5} & $T_c$ & 252.175 & 76.454 & [102.326; 402.025] \\
		  &    & $T_t$ & 302.387 & 77.578 & [150.334; 454.441] \\
		  &    & $T_p$ & 50.212 & 7.882 & [34.763; 65.661] \\
		  \cline{2-6}
		  & \multirow{3}{*}{6} & $T_c$ & 142.322 & 18.270 & [106.513; 178.132] \\
		  &    & $T_t$ & 178.229 & 19.065 & [140.862; 215.595] \\
		  &    & $T_p$ & 35.906 & 3.267 & [29.504; 42.309] \\
		  \cline{2-6}
		  \hline
          \end{tabular}
          }
      \end{center}
  \end{table}

	As Figuras \ref{fig:eth-stderr} e~\ref{fig:wifi-stderr} mostram, respectivamente, os resultados das Tabelas~\ref{tab:eth-metrics} e~\ref{tab:wifi-metrics}. Nelas, é possível comparar diretamente o tempo médio e o intervalo de confiança para os servidores TCP e RMI nos dois testes feitos, para cada operação implementada. Note que em ambos os testes e para todas as operações, o tempo médio do servidor RMI foi maior que o tempo para o servidor TCP. No caso dos testes usando Ethernet, o tempo médio do ponto de vista do cliente para o RMI pode ser de até duas ordens de grandeza maior que do TCP. Como no TCP, no RMI o maior gargalo está na comunicação, como pode ser visto pela semelhança entre as Figuras~\ref{fig:eth-cli-std} e~\ref{fig:eth-com-std}, bem como as Figuras~\ref{fig:wifi-cli-std} e~\ref{fig:wifi-com-std}, onde a semelhança indica que o tempo total do ponto de vista do cliente é dominado pelo tempo de comunicação. Apesar da semelhança, é importante notar que os tempos de processamento no servidor RMI são uma ordem de grandeza maior que os do TCP. Essa diferença é particularmente acentuada para a requisição 6, onde pelas Tabelas~\ref{tab:eth-metrics} e~\ref{tab:wifi-metrics} o tempo de processamento do RMI é cerca de 30 vezes maior que o do TCP. essa diferença se dá pelo nível de abstração do protocolo que realiza inúmeras chamada aninhadas antes de delegar a comunicação via socket para o SO.

  Esperava-se que o tempo de comunicação seria próximo para os dois protocolos, já que os dados sendo transmitidos são essencialmente os mesmos. Porém, deve-se levar em conta que o tempo de comunicação inclui o \emph{marshalling} e \emph{unmarshalling} do objeto \texttt{Reply}, que é feito de forma recursiva para os seus atributos. Objetos complexos como o \texttt{BufferedImage}, são muito custosos de transmitir por causa dos inúmeros outros atributos contidos nele, enquanto no TCP, apenas transmitimos os bytes crus da imagem.

  A carga de trabalho que o protocolo introduz na comunicação fica particularmente nítida ao comparar os tempos total do ponto de vista do cliente para os testes feitos com e sem fio. Em particular, para a requisição 5, note como na Tabela~\ref{tab:eth-metrics} $T_t$ vale $4,479$, enquanto para o Wi-Fi na Tabela~\ref{tab:wifi-metrics}, $T_t$ vale $88,489$. Isso mostra como para o TCP, a mudança de enlace tem uma diferença brutal no desempenho da comunicação. Já no RMI, temos para a requisição 5 pela Tabela~\ref{tab:eth-metrics} que $T_t$ vale $269,419$, enquanto na Tabela~\ref{tab:wifi-metrics} $T_t$ vale $302,387$. Ou seja, a melhora no desempenho ao aumentar a capacidade do enlace é muito menor no RMI que para o TCP, mostrando novamente que a maior parte do custo do protocolo não é na parte de transmissão de dados, mas sim no \emph{overhead} da abstração oferecida para o usuário.

  Apesar do RMI apresentar um desempenho sempre inferior ao TCP, é interessante notar como na rede sem fio, para as requisições com menor volume de dados, o RMI tem um desempenho comparável ao TCP. Em particular, na Figura~\ref{fig:wifi-stderr}, pode-se perceber que par as requisições de 1 a 4, o desempenho é muito similar, sendo sendo até mais consistente que o TCP, como pode ser visto pelas barras de erro menores. Isso mostra que para pequenas mensagens, o perda em desempenho do RMI é aceitável em vista dos benefícios da abstração que ele oferece. Essa propriedade também evidencia que o RMI não é um protocolo indicado para realizar a transmissão de um grande volume de dados, mas é uma alternativa boa quando deseja-se apenas trocar mensagens pequenas.

  \begin{figure}[!ht]
    \centering
    \subfloat[]{
      \label{fig:eth-cli-std}
      \quad\includegraphics[width=0.45\textwidth]{./images/ethernet-client-stderr.pdf}
    }
    \subfloat[]{
      \label{fig:eth-com-std}
      \quad\includegraphics[width=0.45\textwidth]{./images/ethernet-communication-stderr.pdf}
    }
    \caption{Tempo de total do ponto de vista do cliente ($T_t$) médio~\protect\subref{fig:eth-cli-std} e tempo de comunicação ($T_c$) médio~\protect\subref{fig:eth-com-std} com um intervalo de confiança de 95\% para cada requisição nos servidores TCP e RMI conectados através de um enlace Ethernet em uma LAN.}\label{fig:eth-stderr}
  \end{figure}

  \begin{figure}[]
    \centering
    \subfloat[]{
      \label{fig:wifi-cli-std}
      \quad\includegraphics[width=0.45\textwidth]{./images/wifi-client-stderr.pdf}
    }
    \subfloat[]{
      \label{fig:wifi-com-std}
      \quad\includegraphics[width=0.45\textwidth]{./images/wifi-communication-stderr.pdf}
    }
    \caption{Tempo de total do ponto de vista do cliente ($T_t$) médio~\protect\subref{fig:wifi-cli-std} e tempo de comunicação ($T_c$) médio~\protect\subref{fig:wifi-com-std} com um intervalo de confiança de 95\% para cada requisição nos servidores TCP e RMI conectados através de um enlace Wi-Fi em uma LAN.}\label{fig:wifi-stderr}
  \end{figure}
