Tempo de Comunicação
Server type: tcp
Option: 1
Mean: 12.47805116
Standard Deviation: 25.33168434378964
Interval: (-37.17205015382769, 62.12815247382769)
Tempo do Cliente
Server type: tcp
Option: 1
Mean: 12.919906299999997
Standard Deviation: 25.338850796130377
Interval: (-36.744241260415535, 62.58405386041554)
Tempo de Processamento
Server type: tcp
Option: 1
Mean: 0.44185514000000004
Standard Deviation: 0.06770770345918975
Interval: (0.3091480412199881, 0.574562238780012)
Tempo de Comunicação
Server type: tcp
Option: 2
Mean: 12.568225210000001
Standard Deviation: 31.38459985217437
Interval: (-48.945590500261765, 74.08204092026176)
Tempo do Cliente
Server type: tcp
Option: 2
Mean: 13.0668455
Standard Deviation: 31.390376353126193
Interval: (-48.458292152127335, 74.59198315212734)
Tempo de Processamento
Server type: tcp
Option: 2
Mean: 0.4986202900000001
Standard Deviation: 0.08799436030313236
Interval: (0.32615134380586064, 0.6710892361941395)
Tempo de Comunicação
Server type: tcp
Option: 3
Mean: 6.3713382
Standard Deviation: 2.571473455424396
Interval: (1.3312502273681837, 11.411426172631817)
Tempo do Cliente
Server type: tcp
Option: 3
Mean: 8.0275916
Standard Deviation: 2.5843921138712362
Interval: (2.9621830568123766, 13.093000143187622)
Tempo de Processamento
Server type: tcp
Option: 3
Mean: 1.6562533999999998
Standard Deviation: 0.3612389856759118
Interval: (0.9482249880752127, 2.364281811924787)
Tempo de Comunicação
Server type: tcp
Option: 4
Mean: 12.308393319999999
Standard Deviation: 22.400975451462088
Interval: (-31.597518564865695, 56.214305204865695)
Tempo do Cliente
Server type: tcp
Option: 4
Mean: 12.766628300000002
Standard Deviation: 22.418873726160093
Interval: (-31.174364203273782, 56.707620803273784)
Tempo de Processamento
Server type: tcp
Option: 4
Mean: 0.45823498
Standard Deviation: 0.09698235212416201
Interval: (0.2681495698366425, 0.6483203901633575)
Tempo de Comunicação
Server type: tcp
Option: 5
Mean: 85.11821760000001
Standard Deviation: 14.169075895830193
Interval: (57.34682884417283, 112.88960635582718)
Tempo do Cliente
Server type: tcp
Option: 5
Mean: 88.48910900000001
Standard Deviation: 14.337243387483468
Interval: (60.38811196053241, 116.59010603946761)
Tempo de Processamento
Server type: tcp
Option: 5
Mean: 3.3708913999999996
Standard Deviation: 1.3782731498839225
Interval: (0.6694760262275117, 6.072306773772487)
Tempo de Comunicação
Server type: tcp
Option: 6
Mean: 41.914892970000004
Standard Deviation: 33.4295646622746
Interval: (-23.607053768058215, 107.43683970805822)
Tempo do Cliente
Server type: tcp
Option: 6
Mean: 42.972065
Standard Deviation: 33.44233443379654
Interval: (-22.57491049024121, 108.51904049024121)
Tempo de Processamento
Server type: tcp
Option: 6
Mean: 1.05717203
Standard Deviation: 0.1718937628905594
Interval: (0.7202602547345036, 1.3940838052654965)
Tempo de Comunicação
Server type: rmi
Option: 1
Mean: 15.746080240000003
Standard Deviation: 15.383799341798522
Interval: (-14.406166469925099, 45.89832694992511)
Tempo do Cliente
Server type: rmi
Option: 1
Mean: 17.3871715
Standard Deviation: 16.000290068962915
Interval: (-13.973397035167313, 48.74774003516731)
Tempo de Processamento
Server type: rmi
Option: 1
Mean: 1.64109126
Standard Deviation: 3.5140411801966422
Interval: (-5.246429453185419, 8.528611973185418)
Tempo de Comunicação
Server type: rmi
Option: 2
Mean: 14.735078389999998
Standard Deviation: 8.059850262619324
Interval: (-1.0622281247338776, 30.532384904733874)
Tempo do Cliente
Server type: rmi
Option: 2
Mean: 15.90497432
Standard Deviation: 8.035809125670601
Interval: (0.15478843368562245, 31.655160206314378)
Tempo de Processamento
Server type: rmi
Option: 2
Mean: 1.16989593
Standard Deviation: 0.24358469792550536
Interval: (0.6924699220660095, 1.6473219379339905)
Tempo de Comunicação
Server type: rmi
Option: 3
Mean: 14.496170549999997
Standard Deviation: 7.579748578323171
Interval: (-0.36013666351341733, 29.352477763513413)
Tempo do Cliente
Server type: rmi
Option: 3
Mean: 17.12182727
Standard Deviation: 7.59953433427358
Interval: (2.2267399748237846, 32.016914565176215)
Tempo de Processamento
Server type: rmi
Option: 3
Mean: 2.6256567200000003
Standard Deviation: 0.3133423876622188
Interval: (2.0115056401820515, 3.239807799817949)
Tempo de Comunicação
Server type: rmi
Option: 4
Mean: 14.463274660000003
Standard Deviation: 8.735178347345453
Interval: (-2.6576749007970832, 31.58422422079709)
Tempo do Cliente
Server type: rmi
Option: 4
Mean: 15.40043285
Standard Deviation: 8.732399332786784
Interval: (-1.715069842262098, 32.5159355422621)
Tempo de Processamento
Server type: rmi
Option: 4
Mean: 0.93715819
Standard Deviation: 0.24898768556404408
Interval: (0.44914232629447365, 1.4251740537055264)
Tempo de Comunicação
Server type: rmi
Option: 5
Mean: 252.17534063999994
Standard Deviation: 76.45367587111093
Interval: (102.32613593262252, 402.0245453473774)
Tempo do Cliente
Server type: rmi
Option: 5
Mean: 302.38712273
Standard Deviation: 77.57828288224879
Interval: (150.33368828079236, 454.44055717920764)
Tempo de Processamento
Server type: rmi
Option: 5
Mean: 50.21178209
Standard Deviation: 7.882067729322911
Interval: (34.762929340527094, 65.6606348394729)
Tempo de Comunicação
Server type: rmi
Option: 6
Mean: 142.32236737000002
Standard Deviation: 18.27019562277269
Interval: (106.51278394936556, 178.1319507906345)
Tempo do Cliente
Server type: rmi
Option: 6
Mean: 178.22879416999996
Standard Deviation: 19.064592117798693
Interval: (140.86219361911452, 215.5953947208854)
Tempo de Processamento
Server type: rmi
Option: 6
Mean: 35.9064268
Standard Deviation: 3.266746820104136
Interval: (29.50360303259589, 42.309250567404106)
