#include "lib.h"

double elapsed_time(struct timespec t1, struct timespec t2, int micro)
{
  double time_usec;

  time_usec = (t2.tv_sec - t1.tv_sec)*1e6 + (t2.tv_nsec - t1.tv_nsec)*1e-3;
  if (micro) {
    return time_usec;
  }
  else {
    return time_usec*1e-3;
  }
}

long int send_str(int sockfd, char* buffer, int size)
{
  send(sockfd, &size, sizeof(int), 0);
  return send(sockfd, buffer, size, 0);
}


long int recv_str(int sockfd, char* buffer)
{
  int size;
  long int total_bytes = 0, recv_bytes = 0;

  recv(sockfd, &size, sizeof(int), 0);
  do {
    recv_bytes = recv(sockfd, buffer+total_bytes, size-total_bytes, 0);
    total_bytes += recv_bytes;
  } while (total_bytes < size);

  return total_bytes;
}

void recv_profile(int sockfd, char* buffer)
{
  int error;
  long int image_size;
  char filename[200];
  FILE *image_fp;

  /* Break in case query returned null */
  recv(sockfd, &error, sizeof(int), 0);
  if (error > 0) {
    printf("Invalid email address. User not found (%d).\n", error);
    return;
  }

  /* Wait for profile text info */
  recv_str(sockfd, buffer);
  printf("%s", buffer);


  /* Break in case there is no photo */
  recv(sockfd, &error, sizeof(int), 0);
  if (error > 0) {
    printf("No photo available (%d).\n", error);
    return;
  }

  /* Get image filename and open it */
  recv_str(sockfd, buffer);
  sprintf(filename, "client/%s", buffer);
  image_fp = fopen(filename, "w");

  /* Read the entire image into the buffer */
  image_size = recv_str(sockfd, buffer);

  /* Write the entire image to file */
  fwrite(buffer, image_size, 1, image_fp);

  fclose(image_fp);
}
