#include "lib.h"

int check_database_server(int sockfd);

#define PORT "1338"

int main(int argc, char *argv[])
{
    int sockfd;
    struct addrinfo hints, *servinfo, *curr;
    int status;

    // Has to receive the name of the host as argument.
    if(argc != 2){
        fprintf(stderr, "Please state the name of the host\n");
        exit(1);
    }

    // Setting up hints for addrinfo
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;

    // Get server info
    if ((status = getaddrinfo(argv[1], PORT, &hints, &servinfo)) != 0){
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(status));
        return 1;
    }

    // Connect to the first possible
    for (curr = servinfo; curr != NULL; curr = curr->ai_next){

        // Create socket
        if ((sockfd = socket(curr->ai_family, curr->ai_socktype, curr->ai_protocol)) < 0){
            perror("client: socket");
            continue;
        }

        // Connect to server
        if (connect(sockfd, curr->ai_addr, curr->ai_addrlen) < 0){
            close(sockfd);
            perror("client: connect");
            continue;
        }

        break;
    }

    freeaddrinfo(servinfo);

    // Verifying if connected
    if (curr == NULL){
        fprintf(stderr, "client: failed to connect\n");
        return 2;
    }

    printf("Connected\n");

    // Makes requests from server.
    check_database_server(sockfd);

    // All done
    close(sockfd);

    return 0;
}

/**
 * Communicates with server for a total of 6 different operations.
 * Description of the operations is located at the string below (fputs).
 * Depending on the operation, asks for another parameter.
 * When the user wants, ends the connection and exits.
 * @param sockfd Socket File Descriptor - Connection
 * @return time elapsed (usec)
 */
int check_database_server(int sockfd)
{
    int choice = 0;
    char buf[BUFFER_LENGTH];
    struct timespec t1, t2;
    double cli_time;
    char* delim = "===============================";
    int error, i;
    FILE *log;

    // Opens time log
    // TODO: Check if directory exists. If not, create it before opening file
    log = fopen("logs/tcp_cli_time.txt", "a");

    fputs((
          "Options:\n"\
          "1 - List all course graduates;\n"\
          "2 - List skills for city;\n"\
          "3 - Add a new experience;\n"\
          "4 - List all experiences for user;\n"\
          "5 - List everything from everyone;\n"\
          "6 - Get all profile info;\n"\
          "0 - Quit;\n"), stdout);
    printf("Send your choice: ");
    scanf("%d", &choice);

    // Until the user wants to quit (choice = 0).
    while(choice) {

        clock_gettime(CLOCK_ID, &t1);
        send(sockfd, &choice, sizeof(choice), 0);
        clock_gettime(CLOCK_ID, &t2);
        cli_time = elapsed_time(t1, t2, CLIENT_UNIT);

        // Chooses what to ask the user depending on code (info in the string above).
        switch(choice){
            case 1:
                printf("Course: ");
                scanf("\n%[^\n]s", buf);

                clock_gettime(CLOCK_ID, &t1);
                send_str(sockfd, buf, strlen(buf) + 1);
                recv_str(sockfd, buf);
                clock_gettime(CLOCK_ID, &t2);

                printf("%s", buf);

                break;

            case 2:
                printf("City: ");
                scanf("\n%[^\n]s", buf);

                clock_gettime(CLOCK_ID, &t1);
                send_str(sockfd, buf, strlen(buf) + 1);
                recv_str(sockfd, buf);
                clock_gettime(CLOCK_ID, &t2);

                printf("%s", buf);

                break;

            case 3:
                printf("E-Mail Address: ");
                scanf("%s", buf);

                clock_gettime(CLOCK_ID, &t1);
                send_str(sockfd, buf, strlen(buf) + 1);
                clock_gettime(CLOCK_ID, &t2);
                cli_time += elapsed_time(t1, t2, CLIENT_UNIT);

                printf("Experience description: ");
                scanf("\n%[^\n]s", buf);

                clock_gettime(CLOCK_ID, &t1);
                send_str(sockfd, buf, strlen(buf) + 1);
                recv(sockfd, &error, sizeof(int), 0); //TODO: Inserting never returns an error, even if email is invalid
                clock_gettime(CLOCK_ID, &t2);

                if (!error) {
                  printf("Success!\n");
                }
                else {
                  printf("Error: specified email doesn't exist\n"); // Never happens, for now...
                }
                break;

            case 4:
                printf("E-Mail Address: ");
                scanf("%s", buf);

                clock_gettime(CLOCK_ID, &t1);
                send_str(sockfd, buf, strlen(buf)+1);
                recv_str(sockfd, buf);
                clock_gettime(CLOCK_ID, &t2);

                printf("%s", buf);

                break;

            case 5:
                i = 0;
                clock_gettime(CLOCK_ID, &t1);
                recv(sockfd, &error, sizeof(int), 0);
                while(!error) {
                  printf("%.*s Profile %d %.*s\n", 15, delim, ++i, 15, delim);
                  recv_profile(sockfd, buf);
                  recv(sockfd, &error, sizeof(int), 0);
                }
                clock_gettime(CLOCK_ID, &t2);

                break;
            case 6:
                printf("E-Mail Address: ");
                scanf("%s", buf);

                clock_gettime(CLOCK_ID, &t1);
                send_str(sockfd, buf, strlen(buf)+1);
                recv_profile(sockfd, buf);
                clock_gettime(CLOCK_ID, &t2);

                break;
            default:
                        recv_str(sockfd, buf);
                        printf("%s", buf);
                break;
          }

        cli_time += elapsed_time(t1, t2, CLIENT_UNIT);

        if (choice >= 1 && choice <= 6) {
                fprintf(log, "%d %g\n", choice, cli_time);
                fflush(log);
            }

        printf("Send your choice: ");
        scanf("%d", &choice);
    }

    // Ends the connection.
    send(sockfd, &choice, sizeof(int), 0);

    fclose(log);

    return 0;
}
