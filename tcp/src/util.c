#include "util.h"

/*
 * Function: list_course_graduates
 * -------------------------------
 * Lists all people (name, surname) that graduate from a particular course.
 *
 * connection: An initialized and connected MYSQL structure.
 * course: The desired course that we wish to list graduates from.
 *
 * returns: A MySQL query result with the requested data and row seek set to 0.
 */
MYSQL_RES* list_course_graduates(MYSQL* connection, const char* course)
{
  char query[100];
  sprintf(query,
    "SELECT "\
      "name, surname "\
    "FROM "\
      "User "\
    "WHERE "\
      "academic_degree='%s'", course);
  return sql_query(connection, query, 0);
}

/*
 * Function: list_residence_skills
 * -------------------------------
 * Lists all skill descriptions from people that reside on a particular city.
 *
 * connection: An initialized and connected MYSQL structure.
 * residence: City from which to query skills.
 *
 * returns: A MySQL query result with the requested data and row seek set to 0.
 */
MYSQL_RES* list_residence_skills(MYSQL* connection, const char* residence)
{
  char query[100];
  sprintf(query,
    "SELECT "\
      "Skill.description "\
    "FROM "\
      "User JOIN Skill USING (user_id) "\
    "WHERE "\
      "User.residence='%s'", residence);
  return sql_query(connection, query, 0);
}

/*
 * Function: insert_skill
 * -------------------------------
 * Add skill for user with the specified email.
 *
 * connection: An initialized and connected MYSQL structure.
 * email: Email of the user that the skill should be associated with.
 * description: Skill specification
 *
 * returns: A MySQL query result with the requested data and row seek set to 0.
 */
MYSQL_RES* insert_experience(MYSQL* connection, const char* email, const char* description)
{
  char query[200];
  sprintf(query,
    "INSERT INTO "\
      "Experience "\
    "SELECT "\
      "NULL, '%s', user_id "\
    "FROM "\
      "User "\
    "WHERE "\
      "User.email='%s'", description, email);
    return sql_query(connection, query, 0);
}

/*
 * Function: list_user_experience
 * -------------------------------
 * List all experiences for specified user.
 *
 * connection: An initialized and connected MYSQL structure.
 * email: Email of the user to list experiences for.
 *
 * returns: A MySQL query result with the requested data and row seek set to 0.
 */
 MYSQL_RES* list_user_experience(MYSQL* connection, const char* email)
{
  char query[250];
  sprintf(query,
    "SELECT "\
      "Experience.description "\
    "FROM "\
      "User JOIN Experience USING (user_id) "\
    "WHERE "\
      "User.email='%s'", email);
    return sql_query(connection, query, 0);
}

/*
 * Function: list_all
 * -------------------------------
 * List all users' profiles including skills and experience.
 *
 * connection: An initialized and connected MYSQL structure.
 *
 * returns: A MySQL query result with the requested data and row seek set to 0.
 */
MYSQL_RES* list_all(MYSQL* connection)
{
  char query[150];
  sprintf(query,
    "SELECT "\
      "* "\
    "FROM "\
      "User "\
      "LEFT JOIN "\
        "Experience USING (user_id) "\
      "LEFT JOIN "\
        "Skill USING (user_id) "\
    "ORDER BY "\
      "user_id ");
  return sql_query(connection, query, 0);
}



/*
 * Function: list_profile
 * -------------------------------
 * List user's profile including skills and experience.
 *
 * connection: An initialized and connected MYSQL structure.
 * email: Email of the user to list profile for.
 *
 * returns: A MySQL query result with the requested data and row seek set to 0.
 */
MYSQL_RES* list_profile(MYSQL* connection, const char* email)
{
  char query[150];
  sprintf(query,
    "SELECT "\
      "* "\
    "FROM "\
      "User "\
      "LEFT JOIN "\
        "Experience USING (user_id) "\
      "LEFT JOIN "\
        "Skill USING (user_id) "\
    "WHERE "\
      "User.email='%s'", email);
  return sql_query(connection, query, 0);
}


/*
 * Function: query
 * ---------------
 * Queries the database and prints result in a format similar to
 * MySQL's interactive CLI.
 *
 * connection: An initialized and connected MYSQL structure.
 * query: A string that doesn't terminate in ';' with the desired query.
 *
 * returns: A MySQL query result with the requested data and row seek set to 0.
 */
MYSQL_RES* sql_query(MYSQL* connection, const char *query, int verbose)
{
  MYSQL_ROW row;
  MYSQL_RES *result;
  MYSQL_FIELD *fields;
  int num_fields, i;
  char *delimiter;

  if (mysql_query(connection, query))
  {
    fprintf(stderr, "%s\n", mysql_error(connection));
    mysql_close(connection);
    exit(1);
  }

  if ((result = mysql_store_result(connection)) == NULL)
  {

    if (mysql_errno(connection)) {
      fprintf(stderr, "%s\n", mysql_error(connection));
      mysql_close(connection);
      exit(1);
    }
    else {
      return result;
    }
  }

  if (mysql_num_rows(result) == 0) {
    if (verbose)
      printf("Empty query result\n");
    return NULL;
  }

  if (verbose) {
    fields = mysql_fetch_fields(result);
    num_fields = mysql_num_fields(result);

    /* Set max length to the max between filed name and longest value */
    for (i = 0; i < num_fields; i++)
      fields[i].max_length = max(fields[i].max_length, strlen(fields[i].name));

    int result_length = 3; /* first '+' char, '\n', and '\0' */
    for (i = 0; i < num_fields; i++)
      result_length += (int) fields[i].max_length + 3;

    /* Build a delimiter string */
    delimiter = malloc(result_length*sizeof(char));
    int pos = 0;
    for (i = 0; i < num_fields; i++)
    {
      delimiter[pos] = '+';
      for (unsigned int j = 0; j < fields[i].max_length + 2; j++) {
        delimiter[pos+j+1] = '-';
      }
      pos += fields[i].max_length+3;
    }
    delimiter[pos] = '\0';
    strcat(delimiter, "+\n");

    printf("%s", delimiter);

    for (i = 0; i < num_fields; i++)
    {
      printf("| %*s ", -((int) fields[i].max_length), fields[i].name);
    }
    printf("|\n");

    printf("%s", delimiter);

    while ((row = mysql_fetch_row(result)) != NULL)
    {
      for (i = 0; i < num_fields; i++)
      {
        printf("| %*s ", -((int) fields[i].max_length), row[i] != NULL ? row[i] : "NULL");
      }
      printf("|\n");
    }
    printf("%s", delimiter);

    /* Seek to the first row after printing result */
    mysql_data_seek(result, 0);
  }

  return result;
}

/*
 * Function: send_course_graduates
 * ---------------
 * Sends a list of all names of people that graduate from a course.
 *
 * connection: An initialized and connected MYSQL structure.
 * sock_fd: Socket file descriptor through which we'll send profile info.
 * course: Course to list names from.
 *
 * returns: 0 if successful or >0 if an error ocurred
 */
 int send_course_graduates(MYSQL* connection, int sock_fd, char* course, double* proc_time)
 {
   MYSQL_ROW row;
   MYSQL_RES *graduates;
   MYSQL_FIELD *fields;
   int num_fields, i, j = 0;
   char buffer[100];
   char graduates_str[2000];
   struct timespec t1, t2;

   /* Time before message serialization */
   clock_gettime(CLOCK_ID, &t1);

   graduates = list_course_graduates(connection, course);

   if (graduates != NULL) {
     /* Get field info */
     fields = mysql_fetch_fields(graduates);
     num_fields = mysql_num_fields(graduates);

     sprintf(graduates_str, "List of graduates for %s:\n", course);
     while ((row = mysql_fetch_row(graduates)) != NULL)
     {
       sprintf(buffer, "%d) ", ++j);
       strcat(graduates_str, buffer);
       for (i = 0; i < num_fields; i++)
       {
         if (strcmp(fields[i].name, "name") == 0 || strcmp(fields[i].name, "surname") == 0) {
          sprintf(buffer, "%s ", row[i]);
          strcat(graduates_str, buffer);
         }
       }
       strcat(graduates_str, "\n");
     }
   }
   else {
     sprintf(graduates_str, "No graduates for %s\n", course);
   }

   /* Register time after serializing message */
   clock_gettime(CLOCK_ID, &t2);

   /* Measure elapsed time in microseconds */
   *proc_time = elapsed_time(t1, t2, SERVER_UNIT);

   send_str(sock_fd, graduates_str, strlen(graduates_str) + 1);

   return 0;
 }


 /*
  * Function: send_residence_skills
  * ---------------
  * Sends a list of all skills of people living in a particular city.
  *
  * connection: An initialized and connected MYSQL structure.
  * sock_fd: Socket file descriptor through which we'll send profile info.
  * city: Residence that we want to list skills for.
  *
  * returns: 0 if successful or >0 if an error ocurred
  */
  int send_residence_skills(MYSQL* connection, int sock_fd, char* city, double* proc_time)
  {
    MYSQL_ROW row;
    MYSQL_RES *skills;
    MYSQL_FIELD *fields;
    int num_fields, i, j = 0;
    char buffer[100];
    char skills_str[2000];
    struct timespec t1, t2;

    /* Time before message serialization */
    clock_gettime(CLOCK_ID, &t1);

    skills = list_residence_skills(connection, city);

    if (skills != NULL) {
      /* Get field info */
      fields = mysql_fetch_fields(skills);
      num_fields = mysql_num_fields(skills);

      sprintf(skills_str, "List of skills for %s:\n", city);
      while ((row = mysql_fetch_row(skills)) != NULL)
      {
        sprintf(buffer, "%d) ", ++j);
        strcat(skills_str, buffer);
        for (i = 0; i < num_fields; i++)
        {
          if (strcmp(fields[i].name, "description") == 0) {
           sprintf(buffer, "%s ", row[i]);
           strcat(skills_str, buffer);
          }
        }
        strcat(skills_str, "\n");
      }
    }
    else {
      sprintf(skills_str, "No skills registered for %s\n", city);
    }

    /* Register time after serializing message */
    clock_gettime(CLOCK_ID, &t2);

    /* Measure elapsed time in microseconds */
    *proc_time = elapsed_time(t1, t2, SERVER_UNIT);

    send_str(sock_fd, skills_str, strlen(skills_str) + 1);

    return 0;
  }

/*
 * Function: send_user_experience
 * ---------------
 * Sends a list of experiences for the specified user.
 *
 * connection: An initialized and connected MYSQL structure.
 * sock_fd: Socket file descriptor through which we'll send profile info.
 * email: Email of the user we want to list experiences for.
 *
 * returns: 0 if successful or >0 if an error ocurred
 */
 int send_user_experience(MYSQL* connection, int sock_fd, char* email, double* proc_time)
 {
   MYSQL_ROW row;
   MYSQL_RES *experiences;
   MYSQL_FIELD *fields;
   int num_fields, i, j = 0;
   char buffer[100];
   char experiences_str[2000];
   struct timespec t1, t2;

   /* Time before message serialization */
   clock_gettime(CLOCK_ID, &t1);

   experiences = list_user_experience(connection, email);

   if (experiences != NULL) {
     /* Get field info */
     fields = mysql_fetch_fields(experiences);
     num_fields = mysql_num_fields(experiences);

     sprintf(experiences_str, "Experiences for %s:\n", email);
     while ((row = mysql_fetch_row(experiences)) != NULL)
     {
       sprintf(buffer, "%d) ", ++j);
       strcat(experiences_str, buffer);
       for (i = 0; i < num_fields; i++)
       {
         if (strcmp(fields[i].name, "description") == 0) {
          sprintf(buffer, "%s ", row[i]);
          strcat(experiences_str, buffer);
         }
       }
       strcat(experiences_str, "\n");
     }
   }
   else {
     sprintf(experiences_str, "No experiences for user %s\n", email);
   }

   /* Register time after serializing message */
   clock_gettime(CLOCK_ID, &t2);

   /* Measure elapsed time in microseconds */
   *proc_time = elapsed_time(t1, t2, SERVER_UNIT);

   send_str(sock_fd, experiences_str, strlen(experiences_str) + 1);

   return 0;
 }

 /*
  * Function: send_all_profiles
  * ---------------
  * Sends all information on all profiles in the database
  *
  * connection: An initialized and connected MYSQL structure.
  * sock_fd: Socket file descriptor through which we'll send profile info.
  *
  * returns: 0 if successful or >0 if an error ocurred
  */
  int send_all_profiles(MYSQL* connection, int sock_fd, double* proc_time)
  {
    MYSQL_ROW row;
    MYSQL_RES *users;
    char query[150];
    int error = 0;
    double profile_proc_time;
    struct timespec t1, t2;

    clock_gettime(CLOCK_ID, &t1);

    /* Get emails for all users */
    sprintf(query, "SELECT email FROM User");

    users = sql_query(connection, query, 0);

    clock_gettime(CLOCK_ID, &t2);
    *proc_time = elapsed_time(t1, t2, SERVER_UNIT);

    if (users != NULL) {
      while ((row = mysql_fetch_row(users)) != NULL)
      {
        send(sock_fd, &error, sizeof(int), 0);
        send_profile(connection, sock_fd, row[0], &profile_proc_time);
        *proc_time += profile_proc_time;
      }
    }
    error = 1;
    send(sock_fd, &error, sizeof(int), 0);
    return 0;
  }

/*
 * Function: send_profile
 * ---------------
 * Sends all info regarding a profile with a particular email.
 *
 * connection: An initialized and connected MYSQL structure.
 * sock_fd: Socket file descriptor through which we'll send profile info.
 * email: Email of the user to list profile for.
 *
 * returns: 0 if successful or >0 if an error ocurred
 */
int send_profile(MYSQL* connection, int sock_fd, char* email, double* proc_time)
{
  MYSQL_ROW row;
  MYSQL_RES *user, *skills, *experiences;
  MYSQL_FIELD *fields;
  FILE *image_fp;
  char query[150];
  char profile_str[3000];
  char buffer[BUFFER_LENGTH];
  int num_fields, image_size, i;
  int error = 0;
  profile profile_info;
  struct timespec t1, t2;

  /* Time before message serialization */
  clock_gettime(CLOCK_ID, &t1);

  sprintf(query, "SELECT * FROM User WHERE User.email='%s'", email);
  if ((user = sql_query(connection, query, 0)) == NULL) {
    printf("No results for specified email\n");
    error = 1;
  }
  /* Send 0 if query was successful and 1 otherwise */
  send(sock_fd, &error, sizeof(int), 0);
  if (error > 0) {
    clock_gettime(CLOCK_ID, &t2);
    *proc_time = elapsed_time(t1, t2, SERVER_UNIT);
    return error;
  }
  /* Read profile info */
  row = mysql_fetch_row(user);

  /* Get field info */
  fields = mysql_fetch_fields(user);
  num_fields = mysql_num_fields(user);

  /* Set image filepath to an empty string by default */
  strcpy(profile_info.photo, "");

  /* Populate profile struct */
  for (i = 0; i < num_fields; i++) {
    if (strcmp(fields[i].name, "user_id") == 0) {
      profile_info.user_id = atoi(row[i]);
    }
    else if (strcmp(fields[i].name, "email") == 0) {
      strcpy(profile_info.email, row[i]);
    }
    else if (strcmp(fields[i].name, "name") == 0) {
      strcpy(profile_info.name, row[i]);
    }
    else if (strcmp(fields[i].name, "surname") == 0) {
      strcpy(profile_info.surname, row[i]);
    }
    else if (strcmp(fields[i].name, "residence") == 0) {
      strcpy(profile_info.residence, row[i]);
    }
    else if (strcmp(fields[i].name, "academic_degree") == 0) {
      strcpy(profile_info.academic_degree, row[i]);
    }
    else if (strcmp(fields[i].name, "photo") == 0) {
      if (row[i] != NULL) {
        strcpy(profile_info.photo, row[i]);
      }
      else {
        strcpy(profile_info.photo, "");
      }
    }
  }

  /* Query the Skills table for the skills of the specified user */
  sprintf(query, "SELECT * FROM Skill WHERE user_id='%d'", profile_info.user_id);
  skills = sql_query(connection, query, 0);
  profile_info.nr_skills = 0;
  if (skills != NULL) {
    /* Get field info */
    fields = mysql_fetch_fields(skills);
    num_fields = mysql_num_fields(skills);
    while ((row = mysql_fetch_row(skills)) != NULL)
    {
      for (i = 0; i < num_fields; i++)
      {
        if (strcmp(fields[i].name, "description") == 0) {
            strcpy(profile_info.skills[profile_info.nr_skills].description, row[i]);
            profile_info.nr_skills++;
        }
      }
    }
  }

  /* Query the Experiences table for the experiences of the specified user */
  sprintf(query, "SELECT * FROM Experience WHERE user_id='%d'", profile_info.user_id);
  experiences = sql_query(connection, query, 0);
  profile_info.nr_experiences = 0;
  if (experiences != NULL) {
    /* Get field info */
    fields = mysql_fetch_fields(experiences);
    num_fields = mysql_num_fields(experiences);

    while ((row = mysql_fetch_row(experiences)) != NULL)
    {
      for (i = 0; i < num_fields; i++)
      {
        if (strcmp(fields[i].name, "description") == 0) {
            strcpy(profile_info.experiences[profile_info.nr_experiences].description, row[i]);
            profile_info.nr_experiences++;
        }
      }
    }
  }

  serialize_profile(profile_info, profile_str);

  /* Compute processing time in case there is no image */
  clock_gettime(CLOCK_ID, &t2);
  *proc_time = elapsed_time(t1, t2, SERVER_UNIT);

  /* Send all text-based profile info */
  send_str(sock_fd, profile_str, strlen(profile_str) + 1);

  /* In case there is no photo, just continue */
  // printf("Profile photo: %s (%ld)\n", profile_info.photo, strlen(profile_info.photo));

  /* Send 0 if an image is available and 1 otherwise */
  if (strlen(profile_info.photo) == 0) {
    error = 1;
  }
  send(sock_fd, &error, sizeof(int), 0);

  if (error > 0) {
    return error;
  }

  /* Send image filename to client */
  sprintf(buffer, "%s", basename(profile_info.photo));

  send_str(sock_fd, buffer, strlen(buffer) + 1);

  /* Retrieve image size */
  image_fp = fopen(profile_info.photo, "r");
  fseek(image_fp, 0, SEEK_END);
  image_size = ftell(image_fp);
  fseek(image_fp, 0, SEEK_SET);

  /* Read entire image into buffer */
  fread(buffer, 1, image_size, image_fp);

  /* Processing time in case there is an image (add to proc time without)*/
  clock_gettime(CLOCK_ID, &t1);
  *proc_time += elapsed_time(t2, t1, SERVER_UNIT);

  /* Send image to client */
  send_str(sock_fd, buffer, image_size);

  return 0;
}

/*
 * Function: serialize_profile
 * ---------------
 * Serializes a profile struct into a string, ignoring the photo filepath.
 *
 * profile_info: A struct profile type that includes all data read from the
 *               the database
 *
 * returns: A string with all of the profile data to be sent to the client
 */
void serialize_profile(profile profile_info, char *const profile_str)
{
  char experience_buffer[210];
  char skill_buffer[100];

  sprintf(profile_str,
    "Email: %s\n"\
    "Name: %s %s\n"\
    "Residence: %s\n"\
    "Academic Degree: %s\n",
    profile_info.email,
    profile_info.name, profile_info.surname,
    profile_info.residence,
    profile_info.academic_degree
  );

  strcat(profile_str, "Skills: ");
  if (profile_info.nr_skills == 0) {
    strcat(profile_str, "None");
  }
  else {
    for (int i = 0; i < profile_info.nr_skills; i++) {
      sprintf(skill_buffer, "(%d) %s ", i+1, profile_info.skills[i].description);
      strcat(profile_str, skill_buffer);
    }
  }
  strcat(profile_str, "\n");

  strcat(profile_str, "Experiences: ");
  if (profile_info.nr_experiences == 0) {
    strcat(profile_str, "None");
  }
  else {
    for (int i = 0; i < profile_info.nr_experiences; i++) {
      sprintf(experience_buffer, "(%d) %s ", i+1, profile_info.experiences[i].description);
      strcat(profile_str, experience_buffer);
    }
  }
  strcat(profile_str, "\n");
}

/*
 * Function: max
 * ---------------
 * Computes the maximum value between two integers.
 *
 * a: First integer to consider.
 * b: Second integer to consider.
 *
 * returns: The maximum value between the two ints.
 */
int max(int a, int b)
{
  return a > b ? a : b;
}
