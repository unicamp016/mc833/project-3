import os
import glob

metric_to_symbol = {
    "Tempo de Processamento": "$T_p$",
    "Tempo de Comunicação": "$T_c$",
    "Tempo do Cliente": "$T_t$"
}

stats = glob.glob("report/data/*")
for stats_fn in stats:
    name = os.path.basename(stats_fn).split('.')[0]

    # Build a stats dictionary from file
    stats_dict = {}
    with open(stats_fn, "r") as st:
        lines = st.readlines()
        lines = [l.strip() for l in lines]
        for i in range(0, len(lines), 6):
            metric = metric_to_symbol[lines[i]]
            server = lines[i+1].split(':')[-1].upper()
            option = int(lines[i+2].split(':')[-1])
            mean = float(lines[i+3].split(':')[-1])
            stdev = float(lines[i+4].split(':')[-1])

            if server not in stats_dict:
                stats_dict[server] = {}
            if option not in stats_dict[server]:
                stats_dict[server][option] = {}
            if metric not in stats_dict[server][option]:
                stats_dict[server][option][metric] = {}

            stats_dict[server][option][metric]['mean'] = mean
            stats_dict[server][option][metric]['stdev'] = stdev

    # Parse stats dict into latex table syntax
    with open(f"{name}-table.txt", "w") as table:
        for server in stats_dict:
            server_multirow = True
            for option in stats_dict[server]:
                option_multirow = True
                for metric in stats_dict[server][option]:
                    mean = stats_dict[server][option][metric]['mean']
                    stdev = stats_dict[server][option][metric]['stdev']

                    if server_multirow:
                        table.write(f"\\multirow{{18}}{{*}}{{{server}}} ")
                        server_multirow = False

                    if option_multirow:
                        table.write(f"& \\multirow{{3}}{{*}}{{{option}}} ")
                        option_multirow = False
                    else:
                        table.write(f"&    ")

                    ci_lower = max(0,mean - 1.96*stdev)
                    ci_upper = mean + 1.96*stdev
                    table.write(f"& {metric} & {mean:.3f} & {stdev:.3f} & [{ci_lower:.3f}; {ci_upper:.3f}] \\\\\n")
                table.write("\\cline{2-6}\n")
            table.write("\\hline\n")
