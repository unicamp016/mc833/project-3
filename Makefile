# General files and dirs
OUT_DIR = bin
SRC_DIR = src
ASSETS_DIR = assets

# Report variables
REPORT = main
REPORT_DIR = report

.PHONY: build clean reset_db write-report

submit: build
	@mkdir $(OUT_DIR)/code
	@rsync -azP --exclude=".git*" --exclude-from=.gitignore . $(OUT_DIR)/code
	@tar -cz -C $(OUT_DIR) $(REPORT).pdf code -f $(OUT_DIR)/projeto-3.tar.gz
	@rm -rf $(OUT_DIR)/code
		
build: $(OUT_DIR)/$(REPORT).pdf

clean:
	rm -rf $(OUT_DIR)/*

metrics:
	@mkdir -p $(OUT_DIR)
	python3 metrics.py $(ARGS)

write-report: $(REPORT_DIR)/$(REPORT).tex
	latexmk -pdf -pvc -cd $< -jobname=$(OUT_DIR)/$(REPORT)

reset_db:
	cat $(ASSETS_DIR)/profiles.sql | mysql -u root

$(OUT_DIR)/$(REPORT).pdf: $(REPORT_DIR)/$(REPORT).tex
	@mkdir -p $(OUT_DIR)
	latexmk -bibtex -pdf -cd $< -aux-directory=$(OUT_DIR) -output-directory=$(OUT_DIR)
