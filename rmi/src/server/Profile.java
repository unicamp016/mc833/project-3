package server;

import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;

class Profile {
  private boolean valid = true;
  private int id;
  private String email, name, surname, residence, academic_degree;
  private File image_file = null;
  private BufferedImage image = null;
  private ArrayList<String> skills = new ArrayList<String>();
  private ArrayList<String> experiences = new ArrayList<String>();
  private StringBuilder profileInfo = null;

  public Profile(String email, Statement statement) {
    String query;
    ResultSet resultSet;

    this.email = email;
    try {
      /* Query basic profile info */
      query = String.format(
        "SELECT " +
          "* " +
        "FROM " +
          "User " +
        "WHERE " +
          "email='%s'", email);
      resultSet = statement.executeQuery(query);
      if (!resultSet.next()) {
        /* In case the result set is empty */
        this.valid = false;
      } else {
        this.id = resultSet.getInt("user_id");
        this.name = resultSet.getString("name");
        this.surname = resultSet.getString("surname");
        this.residence = resultSet.getString("residence");
        this.academic_degree = resultSet.getString("academic_degree");
        try {
          if (resultSet.getString("photo") != null) {
            this.image_file = new File(resultSet.getString("photo"));
          }
          if (this.image_file != null) {
            this.image = ImageIO.read(this.image_file);
          }
        } catch(IOException ex) {
          ex.printStackTrace();
        }
      }

      /* Query user skills */
      query = String.format(
        "SELECT " +
          "* " +
        "FROM " +
          "Skill " +
        "WHERE " +
          "user_id=%d", id);

      resultSet = statement.executeQuery(query);
      while(resultSet.next()) {
        this.skills.add(resultSet.getString("description"));
      }

      /* Query user experiences */
      query = String.format(
        "SELECT " +
          "* " +
        "FROM " +
          "Experience " +
        "WHERE " +
          "user_id=%d", id);

      resultSet = statement.executeQuery(query);
      while(resultSet.next()) {
        this.experiences.add(resultSet.getString("description"));
      }
    } catch (SQLException ex) {
      ex.printStackTrace();
    }
  }

  public String toString() {
    if (profileInfo == null) {
      profileInfo = new StringBuilder();
      if (!valid) {
        profileInfo.append("No results for specified email " + email + "\n");
      } else {
        profileInfo.append("Email: " + email + "\n");
        profileInfo.append("Name: " + name + " " + surname + "\n");
        profileInfo.append("Residence: " + residence + "\n");
        profileInfo.append("Academic Degree: " + academic_degree + "\n");

        if (skills.size() == 0) {
          profileInfo.append("Skills: None\n");
        } else {
          profileInfo.append("Skills:");
          for (int i = 1; i <= skills.size(); i++) {
            profileInfo.append(" (" + i + ") " + skills.get(i-1));
          }
          profileInfo.append("\n");
        }

        if (experiences.size() == 0) {
          profileInfo.append("Experiences: None\n");
        } else {
          profileInfo.append("Experiences:");
          for (int i = 1; i <= experiences.size(); i++) {
            profileInfo.append(" (" + i + ") " + experiences.get(i-1));
          }
          profileInfo.append("\n");
        }

        if (image == null) {
          profileInfo.append("No photo available\n");
        }
      }
    }
    return profileInfo.toString();
  }

  public String getFilename() {
    return this.image_file.getName();
  }

  public BufferedImage getImage() {
    return this.image;
  }

  public boolean saveImage() {
    if (image != null) {
        try {
          String image_name = image_file.getName();
          String[] tokens = image_name.split("\\.");
          String ext = tokens[tokens.length-1];
          return ImageIO.write(image, ext, new File("bin/" + image_name));
        } catch (IOException ex) {
          ex.printStackTrace();
        }
    }
    return false;
  }

  public boolean isValid() {
    return valid;
  }

}
