package server;

import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import request.ProfileRequest;
import request.Reply;

class Server implements ProfileRequest {
  static final String ERROR = String.valueOf((char) 0);
  private Database database = null;
  private BufferedWriter writer = null;

  public Server() {
    try {
      this.database = new Database();

      /* Initialize log file writer with a 1 MiB write buffer */
      FileWriter fileWriter = new FileWriter("logs/rmi_proc_time.txt", true);
      this.writer = new BufferedWriter(fileWriter, 1 << 20);

    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }

  public BufferedWriter getWriter() {
    return this.writer;
  }

  public Reply satisfyRequest(int requestID, String[] args) {
    Reply rep = null;
    long start_time;
    double proc_time;

    try{
        start_time = System.nanoTime();

        switch(requestID) {
          case 1:
            if (args.length == 1)
              rep = database.listCourseGraduates(args[0]);
            else
              rep = new Reply(ERROR);
            break;

          case 2:
            if (args.length == 1)
              rep = database.listResidenceSkills(args[0]);
            else
              rep = new Reply(ERROR);
            break;

          case 3:
            if (args.length == 2)
              rep = database.insertExperience(args[0], args[1]);
            else
              rep = new Reply(ERROR);
            break;

          case 4:
            if (args.length == 1)
              rep = database.listUserExperience(args[0]);
            else
              rep = new Reply(ERROR);
            break;

          case 5:
            rep = database.getAllProfiles();
            break;

          case 6:
            if (args.length == 1)
              rep = database.getProfile(args[0]);
            else
              rep = new Reply(ERROR);
            break;

          default:
            rep = new Reply("This request hasn't been implemented yet!\n");
            break;
        }

        // Writes log (doesn't flush).
        if (requestID >= 1 && requestID <= 6){
          proc_time = ((double)(System.nanoTime() - start_time))/1000000;
          this.writer.write(Integer.toString(requestID) + " " + Double.toString(proc_time) + "\n");
        }

    } catch (IOException ex){
        ex.printStackTrace();
    }

    return rep;
  }

  public static void main(String[] args) {
    ProfileRequest profileRequest = null;
    Server server = null;

    try {
      server = new Server();
      profileRequest = (ProfileRequest) server;

      /* Add shutdown hook */
      Runtime.getRuntime().addShutdownHook(new Util.ShutdownHook(server));

      System.setProperty("java.rmi.server.hostname", Util.getIP());

      /* Export object at port 1337 */
      ProfileRequest stub = (ProfileRequest) UnicastRemoteObject.exportObject(profileRequest, 0);

      /* Create registry */
      Registry registry = LocateRegistry.createRegistry(1337);

      /* Bind the remote object to the registry using the specified name*/
      registry.rebind("requestServer", stub);
      System.out.println("Request server bound");
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}
