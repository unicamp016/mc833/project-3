package server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.awt.image.BufferedImage;
import request.Reply;

class Database {
  Connection conn = null;
  Statement statement = null;
  ResultSet resultSet = null;

  public Database() {
    try {
      conn =
        DriverManager.getConnection(
          "jdbc:mysql://localhost/profiles",    // database url
          "root",                               // username
          ""                                    // password
        );
      statement = conn.createStatement();
    } catch (SQLException ex) {
      System.out.println("SQLException: " + ex.getMessage());
      System.out.println("SQLState: " + ex.getSQLState());
      System.out.println("VendorError: " + ex.getErrorCode());
      ex.printStackTrace();
    }
  }

  public Reply listCourseGraduates(String course) {
    StringBuilder res = new StringBuilder();
    int rows = 0;

    String query = String.format(
      "SELECT " +
        "name, surname " +
      "FROM " +
        "User " +
      "WHERE " +
        "academic_degree='%s'", course);

    try {
      resultSet = statement.executeQuery(query);

      if (!resultSet.next()) {
        /* In case the result set is empty */
        res.append("No graduates for " + course + "\n");
      } else {
        /* Create list of names for graduates */
        res.append("List of graduates for " + course + ":\n");
        do {
          res.append(++rows).append(") ");
          res.append(resultSet.getString("name") + " ");
          res.append(resultSet.getString("surname") + "\n");
        } while (resultSet.next());
      }
    } catch (SQLException ex) {
      ex.printStackTrace();
    }
    return new Reply(res.toString());
  }

  public Reply listResidenceSkills(String city) {
    StringBuilder res = new StringBuilder();
    int rows = 0;

    String query = String.format(
      "SELECT " +
        "description " +
      "FROM " +
        "User NATURAL JOIN Skill " +
      "WHERE " +
        "residence='%s'", city);

    try {
      resultSet = statement.executeQuery(query);

      if (!resultSet.next()) {
        /* In case the result set is empty */
        res.append("No skills registered for " + city + "\n");
      } else {
        /* Create list of names for graduates */
        res.append("List of skills for " + city + ":\n");
        do {
          res.append(++rows).append(") ");
          res.append(resultSet.getString("description") + "\n");
        } while (resultSet.next());
      }
    } catch (SQLException ex) {
      ex.printStackTrace();
    }
    return new Reply(res.toString());
  }

  public Reply insertExperience(String email, String description) {
    StringBuilder res = new StringBuilder();
    byte affectedRows = 0;

    String query = String.format(
      "INSERT INTO " +
        "Experience (description, user_id) " +
      "VALUES " +
        "('%s', (SELECT user_id FROM User WHERE email='%s'))", description, email);

    try {
      affectedRows = (byte) statement.executeUpdate(query);
      res.append((char)affectedRows);
    } catch (SQLException ex) {
      /* In case specified email doesn't exist or description is NULL, return 0 */
      if (ex.getClass() == SQLIntegrityConstraintViolationException.class) {
        res.append((char) 0);
      } else {
        ex.printStackTrace();
      }
    }

    return new Reply(res.toString());
  }

  public Reply listUserExperience(String email) {
    StringBuilder res = new StringBuilder();
    int rows = 0;

    String query = String.format(
      "SELECT " +
        "description " +
      "FROM " +
        "User NATURAL JOIN Experience " +
      "WHERE " +
        "email='%s'", email);

    try {
      resultSet = statement.executeQuery(query);
      if (!resultSet.next()) {
        /* In case the result set is empty */
        res.append("No experiences for user " + email + "\n");
      } else {
        /* Create list of experiences for profile */
        res.append("Experiences for " + email + ":\n");
        do {
          res.append(++rows).append(") ");
          res.append(resultSet.getString("description") + "\n");
        } while (resultSet.next());
      }
    } catch (SQLException ex) {
      ex.printStackTrace();
    }
    return new Reply(res.toString());
  }

  public Reply getProfile(String email) {
    Profile profile = new Profile(email, this.statement);
    Reply reply = new Reply(profile.toString());
    if (profile.getImage() != null) {
      reply.addFilename(profile.getFilename());
      reply.addImage(profile.getImage());
    }
    return reply;
  }

  public Reply getAllProfiles() {
    String query = "SELECT email FROM User";
    StringBuilder res = new StringBuilder();
    ArrayList<Reply> all_replies = new ArrayList<Reply>();
    ArrayList<String> emails = new ArrayList<String>();
    ArrayList<BufferedImage> profile_photo = null;
    ArrayList<String> profile_filenames = null;
    Reply reply = new Reply();

    try {
      resultSet = statement.executeQuery(query);
      while(resultSet.next()) {
        emails.add(resultSet.getString("email"));
      }
      for (int i = 0; i < emails.size(); i++) {
        all_replies.add(getProfile(emails.get(i)));
      }
    } catch (SQLException ex) {
      ex.printStackTrace();
    }

    for (int i = 0; i < all_replies.size(); i++) {
      res.append("=============== Profile " + (i+1) + " ===============\n");
      res.append(all_replies.get(i).getText());
      profile_photo = all_replies.get(i).getImages();
      profile_filenames = all_replies.get(i).getFilenames();
      if (profile_photo.size() > 0) {
        reply.addFilename(profile_filenames.get(0));
        reply.addImage(profile_photo.get(0));
      }
    }
    reply.setText(res.toString());
    return reply;
  }

}
