package server;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.io.IOException;
import java.io.FileWriter;

public class Util {

  public static String getIP() throws UnknownHostException {
    DatagramSocket socket = null;

    try {
      socket = new DatagramSocket();
      socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
      return socket.getLocalAddress().getHostAddress();
    } catch(Exception ex) {
        return InetAddress.getLocalHost().getHostAddress();
    }
  }

  public static class ShutdownHook extends Thread {
    Server server = null;
    public ShutdownHook(Server server) {
      this.server = server;
    }

    @Override
    public void run() {
      try {
        this.server.getWriter().close();
      } catch (IOException ex) {
        ex.printStackTrace();
      } finally {
        System.out.println("\nFinishing execution... Goodbye!");
        Runtime.getRuntime().halt(0);
      }
    }
  }
}
