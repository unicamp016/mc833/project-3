package request;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ProfileRequest extends Remote {
  Reply satisfyRequest(int requestID, String[] args) throws RemoteException;
}
