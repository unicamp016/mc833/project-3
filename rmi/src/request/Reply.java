package request;

import java.io.Serializable;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;

public class Reply implements Serializable{
  private String text = null;
  private ArrayList<String> filenames = null;
  private transient ArrayList<BufferedImage> images = null;

  public Reply() {
    this.filenames = new ArrayList<String>();
    this.images = new ArrayList<BufferedImage>();
  }

  public Reply(String text) {
    this();
    this.text = text;
  }

  public void addImage(BufferedImage image) {
    this.images.add(image);
  }

  public ArrayList<BufferedImage> getImages() {
    return this.images;
  }

  public void addFilename(String filename) {
    this.filenames.add(filename);
  }

  public ArrayList<String> getFilenames() {
    return this.filenames;
  }

  public String getText() {
    return this.text;
  }

  public void setText(String text) {
    this.text = text;
  }

  private void writeObject(ObjectOutputStream out) throws IOException {
    ByteArrayOutputStream buffer = null;

    /* Marshal static objects */
    out.defaultWriteObject();


    out.writeInt(this.images.size());
    for (BufferedImage eachImage : images) {
      buffer = new ByteArrayOutputStream();
      ImageIO.write(eachImage, "jpg", buffer);

      out.writeInt(buffer.size());
      buffer.writeTo(out);
    }
  }

  private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
    int imageCount = 0, imageSize = 0;
    byte[] buffer = null;
    BufferedImage image;

    /* Unmarshal static objects */
    in.defaultReadObject();

    imageCount = in.readInt();
    images = new ArrayList<BufferedImage>(imageCount);
    for (int i=0; i < imageCount; i++) {
      imageSize = in.readInt();
      buffer = new byte[imageSize];
      in.readFully(buffer);

      image = ImageIO.read(new ByteArrayInputStream(buffer));
      this.images.add(image);
    }
  }
}
