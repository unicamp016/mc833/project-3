package client;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.RemoteException;
import java.util.Scanner;
import java.util.ArrayList;
import java.io.File;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import request.ProfileRequest;
import request.Reply;

public class Client {
  Registry registry = null;
  ProfileRequest requestServer = null;
  BufferedWriter writer = null;

  public Client(String url) {
    try {
      /* Look for registry on localhost 5000 */
      this.registry = LocateRegistry.getRegistry(url, 1337);

      /* Get remote object reference through registry */
      this.requestServer = (ProfileRequest) this.registry.lookup("requestServer");

    } catch(Exception ex) {
      ex.printStackTrace();
    }
  }

  public Reply submitRequest(int requestID, String[] args) throws RemoteException {
    return this.requestServer.satisfyRequest(requestID, args);
  }

  public static void saveImages(Reply reply) {
    ArrayList<String> filenames = reply.getFilenames();
    ArrayList<BufferedImage> images = reply.getImages();

    try {
      for (int i = 0; i < images.size(); i++) {
        ImageIO.write(images.get(i), "jpg", new File("client/" + filenames.get(i)));
      }
    } catch(IOException ex) {
      ex.printStackTrace();
    }
  }

  public static void main(String[] args) {
    ArrayList<String> requestArgs = new ArrayList<String>();
    String url = null;
    Reply reply = null;
    long start_time;
    double cli_time;
    BufferedWriter writer = null;

    if (args.length > 0) {
      url = args[0];
    }

    Client client = new Client(url);
    Scanner scan = new Scanner(System.in);

    System.out.print(
      "Options:\n" +
      "1 - List all course graduates;\n" +
      "2 - List skills for city;\n" +
      "3 - Add a new experience;\n" +
      "4 - List all experiences for user;\n" +
      "5 - List everything from everyone;\n" +
      "6 - Get all profile info;\n" +
      "0 - Quit.\n");

    try {

      FileWriter fileWriter = new FileWriter("logs/rmi_cli_time.txt", true);
      writer = new BufferedWriter(fileWriter, 1 << 20);

      while (true) {
        System.out.print("Send your choice: ");
        int requestID = Integer.parseInt(scan.nextLine());
        requestArgs.clear();
        switch (requestID) {
          case 0:
           System.out.println("Goodbye!");
           writer.close();
           System.exit(0);
          case 1:
            System.out.print("Course: ");
            requestArgs.add(scan.nextLine());
            start_time = System.nanoTime();
            reply = client.submitRequest(requestID, requestArgs.toArray(new String[0]));
            break;
          case 2:
            System.out.print("City: ");
            requestArgs.add(scan.nextLine());
            start_time = System.nanoTime();
            reply = client.submitRequest(requestID, requestArgs.toArray(new String[0]));
            break;
          case 3:
            System.out.print("Email: ");
            requestArgs.add(scan.nextLine());
            System.out.print("Experience description: ");
            requestArgs.add(scan.nextLine());
            start_time = System.nanoTime();
            reply = client.submitRequest(requestID, requestArgs.toArray(new String[0]));
            /* TODO: print a special message in case of an error */
            break;
          case 4:
            System.out.print("Email: ");
            requestArgs.add(scan.nextLine());
            start_time = System.nanoTime();
            reply = client.submitRequest(requestID, requestArgs.toArray(new String[0]));
            break;
          case 5:
            start_time = System.nanoTime();
            reply = client.submitRequest(requestID, requestArgs.toArray(new String[0]));
            saveImages(reply);
            break;
          case 6:
            System.out.print("Email: ");
            requestArgs.add(scan.nextLine());
            start_time = System.nanoTime();
            reply = client.submitRequest(requestID, requestArgs.toArray(new String[0]));
            saveImages(reply);
            break;
          default:
            System.out.println("Request hasn't been implemented yet");
            continue;
        }


        if (requestID == 3) {
          System.out.println("Success!"); // TODO: Handle error afterwards
        } else {
          System.out.print(reply.getText());
        }

        // Writes log in miliseconds.
        if (requestID >= 1 && requestID <= 6){
            cli_time = ((double)(System.nanoTime() - start_time))/1000000;
            writer.write(Integer.toString(requestID) + " " + Double.toString(cli_time) + "\n");
            writer.flush();
        }
      }
    } catch(RemoteException ex) {
      ex.printStackTrace();
    } catch(IOException io){
      io.printStackTrace();
    }
  }
}
