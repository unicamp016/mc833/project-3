DROP DATABASE IF EXISTS profiles;
CREATE DATABASE profiles;
USE profiles;

DROP TABLE IF EXISTS `Skill`;
DROP TABLE IF EXISTS `Experience`;
DROP TABLE IF EXISTS `User`;

CREATE TABLE User (
    user_id int NOT NULL AUTO_INCREMENT,
    email varchar(60) NOT NULL,
    name varchar(30) NOT NULL,
    surname varchar(30) NOT NULL,
    residence varchar(30),
    academic_degree varchar(30),
    photo varchar(60),
    PRIMARY KEY (user_id)
);


CREATE TABLE Skill (
    skill_id int NOT NULL AUTO_INCREMENT,
    description varchar(90) NOT NULL,
    user_id int NOT NULL,
    PRIMARY KEY (skill_id),
    FOREIGN KEY (user_id) REFERENCES User(user_id)
);

CREATE TABLE Experience (
    exp_id int NOT NULL AUTO_INCREMENT,
    description varchar(200) NOT NULL,
    user_id int NOT NULL,
    PRIMARY KEY (exp_id),
    FOREIGN KEY (user_id) REFERENCES User(user_id)
);

INSERT INTO User
VALUES
  (1, 'panda.prudencio@gmail.com', 'Panda', 'Prudencio', 'Campinas', 'Computer Engineering', NULL),
  (2, 'mimao.vendra@hotmail.com', 'Mimao', 'Vendramini', 'Ouroeste', 'Computer Engineering', NULL),
  (3, 'tutti.nervouser@yahoo.com', 'Tutti', 'Nervouser', 'Berlin', 'Archeology', 'assets/images/tutti.jpg'),
  (4, 'juja.princess@gmail.com', 'Juja', 'Miola', 'Campinas', 'Archeology', 'assets/images/juja.jpg');

INSERT INTO Skill
VALUES
  (NULL, 'League of Legends', 1),
  (NULL, 'Deep Learning', 1),
  (NULL, 'Robotics', 1),
  (NULL, 'Hand Reading', 2),
  (NULL, 'Pharmaceutics', 2),
  (NULL, 'Growling', 3),
  (NULL, 'Gnawing', 3),
  (NULL, 'Eating', 4);

INSERT INTO Experience
VALUES
  (NULL, 'Achieved Rank 1 in the League of Legends Brazilian server', 1),
  (NULL, 'Finished Bioshock Infinite in Hard', 1),
  (NULL, 'Completed course in paleatal care', 2),
  (NULL, 'Gnawed at a 1kg bone for 1 day straight', 3);
